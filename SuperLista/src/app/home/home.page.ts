import { Component } from '@angular/core';
import { ServSuperHeroService } from '../serv-super-hero.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  heroID : any;
  heroName : any;
  heroImage : any;
  heroPowerData :any;
  heroBiography: any;
  heroAppearance : any;
  heroWork : any;
  heroConnections : any;
  constructor(public service:ServSuperHeroService) {}
  getBatman(){
    this.heroID = '69';
    this.service.gethero(this.heroID).subscribe((data)=>{
      this.heroName = data['name'];
      console.log(this.heroName);
      this.heroImage = data['image'];
      console.log(this.heroImage);
      this.heroPowerData = data['powerstats'];
      console.log(this.heroPowerData);
      this.heroBiography = data['biography'];
      console.log(this.heroBiography);
      this.heroAppearance = data['appearance'];
      console.log(this.heroAppearance);
      this.heroWork = data['work'];
      console.log(this.heroWork);
      this.heroConnections = data['connections'];
      console.log(this.heroConnections);
    })
  }
}
