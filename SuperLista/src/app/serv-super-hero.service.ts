import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class ServSuperHeroService {
  constructor(private http:HttpClient,
              private storage: Storage) { }
  
  gethero(heroID){
    console.log(heroID);
    console.log('https://superheroapi.com/api/543528076525019/'+heroID);
    return this.http.get('https://superheroapi.com/api/543528076525019/'+heroID);
  }
}
